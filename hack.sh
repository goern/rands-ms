#!/bin/bash

export CGO_ENABLED=0

echo -n "restoring dependencies using godep... "
go get github.com/tools/godep
go get golang.org/x/tools/cmd/goimports
godep restore
echo "done."

echo -n "building statically linked binary... "
cd src/
go build --ldflags '-extldflags "-static"' -o ../rands-ms
cd ../
strip rands-ms
echo "done."
