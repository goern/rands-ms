FROM scratch

MAINTAINER goern@redhat.com

LABEL Vendor="Red Hat" \
      License="GPLv3" \
      Version="0.1.0"

LABEL io.k8s.description="A random number generating microservice" \
      io.k8s.display-name="rands-ms 0.1.0" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="rands-ms,0.1.0"

ENV container=docker

LABEL RUN="docker run --detach --publish 8080:8080 rands-ms" \
      Build="docker build --rm --tag rands-ms ."

ADD rands-ms /rands-ms

EXPOSE 8080

ENTRYPOINT [ "/rands-ms" ]
