package main

import (
	"math/rand"
	"strconv"

	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	v1 := router.Group("/v1")
	{
		// Ping test
		v1.GET("/ping", func(c *gin.Context) {
			c.String(200, "pong")
		})

		// Version
		v1.GET("/", func(c *gin.Context) {
			c.String(200, "This is rands-ms 0.1.0")
		})

		// Get user value
		v1.GET("/rand", func(c *gin.Context) {
			max := c.DefaultQuery("max", "4096")

			i, err := strconv.Atoi(max)
			if err != nil {
				c.JSON(http.StatusBadRequest, gin.H{"error": "max was to large"})
			} else {
				c.JSON(http.StatusOK, gin.H{"rand": rand.Intn(i), "max": i})
			}
		})
	}

	// Listen and Server in 0.0.0.0:8080
	router.Run(":8080")
}
